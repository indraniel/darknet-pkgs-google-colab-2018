This is a repo containing debian packages to install darknet / YOLOv3 on Google Colaboratory circa December 2018.

It contains 2 packages:

1. `cudnn.tar.gz` -- based on CUDA v9.2 (v7.3.1.20 edition)
2. `vegetation-darknet-google-colab-2018_20181203-1ubuntu18.04.deb` -- darknet on github as of December 1, 2018 built against CUDA 9.2, and the above mentioned cuDNN package.  _This package is only meant for YOLOv3 training.  Not detecting!_

The `pkg-builder` directory contains the scripts to build and package darknet for training use on google cloud.  It was used to make `vegetation-darknet-google-colab-2018_20181203-1ubuntu18.04.deb`.

These packages are meant to be downloaded and installed in the Google Colaboratory session of interest.
