#!/bin/bash

# This script is meant to be run in the google colab environment.

# the directory this script is located at
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

function die {
    local timestamp=$(date +"%Y-%m-%d %T")
    echo -e "${RED}[ ${timestamp} ] ERROR: $@ ${NC}" >&2
    kill -s TERM ${TOP_PID}
}

function log {
    local timestamp=$(date +"%Y-%m-%d %T")
    echo -e "${GREEN}---> [ ${timestamp} ] $@ ${NC}" >&2
}

# this is the root directory google colab initially places you in
ROOT=/content 

# the version of darknet we are building
DARKNET_VERSION="20181203"

# this is the directory where we will eventually place darknet
INSTALL_DIR=/opt/darknet-${DARKNET_VERSION}/bin
ROOT_DARKNET_INSTALL_DIR=/opt/darknet-${DARKNET_VERSION}

# start from the base directory
log "Start at ${ROOT}"
cd ${ROOT}; 

log "Install prerequisites"
set -o xtrace;
apt-get install -y \
    build-essential \
    libcurl4-openssl-dev \
    ca-certificates \
    apt-transport-https \
    debhelper \
    git \
    curl \
    zlib1g-dev
set +o xtrace;

log "install all the cuda libraries (apt-get install -y cuda)"
apt-get install -y cuda

log "install the cudnn software (7.3.1.20)"
cp -v ${SCRIPT_DIR}/../cudnn.tar.gz ${ROOT}
cd ${ROOT} && tar zxvf cudnn.tar.gz
cp -v ${ROOT}/cuda/include/cudnn.h /usr/local/cuda-9.2/targets/x86_64-linux/include
cp -v ${ROOT}/cuda/lib64/libcudnn* /usr/local/cuda-9.2/targets/x86_64-linux/lib

log 'git clone darknet -- https://github.com/indraniel/darknet.git'
cd ${ROOT}
git clone https://github.com/indraniel/darknet.git darknet

COLAB_BRANCH="google-colab-training-2018"
log "(1) checkout the customized darknet branch: ${COLAB_BRANCH}"
log "(2) build darknet"
cd ${ROOT}/darknet \
    && git checkout -b google-colab-training-2018 origin/google-colab-training-2018 \
    && make

if [ ! -f ${ROOT}/darknet/darknet ]; then
    die "[err] Did not build the darknet software!"
fi


log "create the darknet install directory: ${INSTALL_DIR}"
mkdir -p ${INSTALL_DIR} || die "did not create ${INSTALL_DIR}"

log "copy over the relevant darknet files to ${INSTALL_DIR}"
cp -v ${ROOT}/darknet/darknet ${INSTALL_DIR} || die "did not copy over darknet executable"
cp -v ${ROOT}/darknet/darknet-colab ${INSTALL_DIR} || die "did not copy over darknet-colab file"

log "build debian package"

DEB_PKG_NAME=vegetation-darknet-google-colab-2018
DEB_BUILD_DIR=${SCRIPT_DIR}/deb-build
UBUNTU_EDITION=$(source /etc/lsb-release; echo $DISTRIB_RELEASE)
DEB_RELEASE_VERSION="1ubuntu${UBUNTU_EDITION}"
DEB_PKG=${DEB_PKG_NAME}_${DARKNET_VERSION}-${DEB_RELEASE_VERSION}.deb
DEB_PKG_INSTALL_DIR=/opt/darknet-${DARKNET_VERSION}
RELEASE_DIR=${SCRIPT_DIR}

# setup the debian control file
DEB_CONTROL_FILE="\
Package: vegetation-darknet-google-colab-2018
Architecture: amd64
Section: science
Maintainer: Vegetation Group <deepimage.learn@gmail.com>
Priority: optional
Depends: cuda, cuda-9-2
Description: Custom darknet built against CUDA 9.2 and cuDNN 7.3.1.20 for Google Colab (Dec-2018)
Version: ${DARKNET_VERSION}
"

# setup the debian postrm file
DEB_POSTRM_FILE="\
#!/bin/bash

BASE=${DEB_PKG_INSTALL_DIR}

if [ -e \${BASE} ]; then
    rm -rfv \${BASE}
fi
"

if [ -e "${SCRIPT_DIR}/${DEB_PKG}" ]; then
    echo "Removing prior existing debian package ${SCRIPT_DIR}/${DEB_PKG}"
    rm ${SCRIPT_DIR}/${DEB_PKG}
fi

if [ -d "${DEB_BUILD_DIR}" ]; then
    echo "Removing prior existing debian package build directory: ${DEB_BUILD_DIR}"
    rm -rf ${DEB_BUILD_DIR}
fi

mkdir -p ${DEB_BUILD_DIR}/${DEB_PKG_INSTALL_DIR}

# debian package pre-requisites
echo "${DEB_CONTROL_FILE}" > ${DEB_BUILD_DIR}/control
echo "${DEB_POSTRM_FILE}" > ${DEB_BUILD_DIR}/postrm
echo 2.0 > ${DEB_BUILD_DIR}/debian-binary

# dump in the files of interest
mkdir -p ${DEB_BUILD_DIR}/${DEB_PKG_INSTALL_DIR}
for p in bin; do
    /bin/cp -rv ${ROOT_DARKNET_INSTALL_DIR}/${p} ${DEB_BUILD_DIR}/${DEB_PKG_INSTALL_DIR}
done

# create the underlying tars of the debian package
tar cvzf ${DEB_BUILD_DIR}/data.tar.gz --owner=0 --group=0 -C ${DEB_BUILD_DIR} opt
tar cvzf ${DEB_BUILD_DIR}/control.tar.gz -C ${DEB_BUILD_DIR} control postrm

# assemble the formal "deb" package
cd ${DEB_BUILD_DIR} && \
    ar rc ${DEB_PKG} debian-binary control.tar.gz data.tar.gz && \
    mv ${DEB_PKG} ${RELEASE_DIR}
